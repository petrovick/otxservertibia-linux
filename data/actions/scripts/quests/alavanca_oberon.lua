local config = {
    timeToWait = {2, 'hour'}, -- tempo para usar novamente após atingir o max_times
    maxPlayers = 5, -- máximo de players dentro da área
    room = {fromPos = Position(33146, 31958, 7), toPos = Position(33160, 31971, 7)}, -- posição do canto superior esquerdo, posição do canto inferior direito da sala
    newPos = Position(33153, 31968, 7), -- posição para onde o player será teleportado ao entrar
    stone = {id = 424, pos = Position(33146, 31988, 7)}, -- id da pedra, posição
	stone1 = {id = 424, pos = Position(33147, 31988, 7)}, -- id da pedra, posição
	stone2 = {id = 424, pos = Position(33148, 31988, 7)}, -- id da pedra, posição
	stone3 = {id = 424, pos = Position(33149, 31988, 7)}, -- id da pedra, posição
	stone3 = {id = 424, pos = Position(33150, 31988, 7)}, -- id da pedra, posição
    timeToKick = {3, 'min'}, -- tempo para ser kikado da sala
    kickPos = Position(33145, 31987, 7), -- quando kikados da área, o player vai para essa posição
}
function onUse(player, item, fromPosition, target, toPosition, isHotkey)
    if player:hasExhaustion(1945) then
        player:sendTextMessage(MESSAGE_EVENT_ADVANCE, "Você pode usar novamente em " .. os.date("%d %B %Y %X", player:getStorageValue(1945))..".")
        return true
    end
    if player:getStorageValue(1945) == config.maxTimes then
        player:setStorageValue(1945, -1)
    end
    if #getPlayersInArea(config.room.fromPos, config.room.toPos) >= config.maxPlayers then
        player:getPosition():sendMagicEffect(CONST_ME_POFF)
        player:sendTextMessage(MESSAGE_EVENT_ADVANCE, 'Sinto muito, mais já tem jogadores lá dentro.')
        return true
    end
    local stone = Tile(config.stone.pos):getItemById(config.stone.id)
    if stone then
        stone:getPosition():sendMagicEffect(CONST_ME_POFF)
        stone:remove()
    end
	local stone1 = Tile(config.stone1.pos):getItemById(config.stone1.id)
    if stone1 then
        stone1:getPosition():sendMagicEffect(CONST_ME_POFF)
        stone1:remove()
    end
	local stone2 = Tile(config.stone2.pos):getItemById(config.stone2.id)
    if stone2 then
        stone2:getPosition():sendMagicEffect(CONST_ME_POFF)
        stone2:remove()
    end
	local stone3 = Tile(config.stone3.pos):getItemById(config.stone3.id)
    if stone3 then
        stone3:getPosition():sendMagicEffect(CONST_ME_POFF)
        stone3:remove()
    end
	local stone4 = Tile(config.stone4.pos):getItemById(config.stone4.id)
    if stone4 then
        stone4:getPosition():sendMagicEffect(CONST_ME_POFF)
        stone4:remove()
    end
    player:teleportTo(config.newPos)
    player:getPosition():sendMagicEffect(CONST_ME_TELEPORT)
    addEvent(kickFromArea, mathtime(config.timeToKick) * 1000, player.uid)
    player:say("você veio ao meu mundo.", TALKTYPE_MONSTER_SAY)
    player:say("Muhahaha....", TALKTYPE_MONSTER_SAY)
    return true
end
 
function getPlayersInArea(fromPos, toPos)
    local players, playerss = {}, Game.getPlayers()
    for i = 1, #playerss do
        local player = playerss[i]
        if isInRange(player:getPosition(), fromPos, toPos) then
            table.insert(players, player)
        end
    end
    return players
end
 
function mathtime(table) -- by dwarfer
    local unit = {"sec", "min", "hour", "day"}
    for i, v in pairs(unit) do
        if v == table[2] then
            return table[1]*(60^(v == unit[4] and 2 or i-1))*(v == unit[4] and 24 or 1)
        end
    end
    return error("Bad declaration in mathtime function.")
end
 
function kickFromArea(cid)
    local stone = Tile(config.stone.pos):getItemById(config.stone.id)
    if not stone then
        Game.createItem(config.stone.id, 1, config.stone.pos)
    end
    local player = Player(cid)
    if player and isInRange(player:getPosition(), config.room.fromPos, config.room.toPos) then
        player:teleportTo(config.kickPos)
        player:say("Poff...", TALKTYPE_MONSTER_SAY)
        player:getPosition():sendMagicEffect(CONST_ME_TELEPORT)
    end
end