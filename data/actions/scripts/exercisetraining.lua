﻿local skills = {
	[32350] = {id=SKILL_SWORD,voc=4},
	[32351] = {id=SKILL_AXE,voc=4},
	[32352] = {id=SKILL_CLUB,voc=4},
	[32353] = {id=SKILL_DISTANCE,voc=3,range=CONST_ANI_SIMPLEARROW},
	[32354] = {id=SKILL_MAGLEVEL,voc=2,range=CONST_ANI_ENERGY},
	[32355] = {id=SKILL_MAGLEVEL,voc=1,range=CONST_ANI_FIRE},
}

------- CONFIG -----//
local d﻿ummies = {32357,32358}
local skill_gain = 1 -- per hit
local gain_stamina = 60

local function start_train(pid,start_pos,itemid,fpos)
	print(string.format("%s", ""))
	local player = Player(pid)
	if player ~= nil then
		local pos_n = player:getPosition()
		print(string.format("if player ~= nil then : = %s", player))
		if start_pos:getDistance(pos_n) == 0 and getTilePzInfo(pos_n) then
			print(string.format("layer:getItemCount(itemid) : = %s", itemid))
			local itemidAux = itemid;
			if player:getItemCount(itemid) >= 1 then
				local exercise = player:getItemById(itemid,true)
				print(string.format("local exercise = player:getItemById(itemid,true) : = %s", exercise))
				
				--exercise:setAttribute(ITEM_ATTRIBUTE_CHARGES, 500)

				if exercise:isItem() then
					print(string.format("exercise:isItem() : = %s", exercise:isItem()))
					if exercise:hasAttribute(ITEM_ATTRIBUTE_CHARGES) then
						local charges_n = exercise:getAttribute(ITEM_ATTRIBUTE_CHARGES)
							print(string.format("charges_n : = %d", charges_n))

						if charges_n >= 1 then
							print(string.format("charges_n >= 1 : = %s", charges_n))
							exercise:setAttribute(ITEM_ATTRIBUTE_CHARGES, (charges_n-1))

							itemid = itemidAux;
							local required = 0
							local currently = 0
							local voc = player:getVocation()
							print(string.format("ANTES item﻿id == SKILL_MAGLEVEL: = %s", item﻿id))
							print(string.format("ANTES skills[item﻿id] == SKILL_MAGLEVEL: = %s", skills[item﻿id],SKILL_MAGLEVEL))

							if skills[item﻿id].id == SKILL_MAGLEVEL then
								print(string.format("skills[item﻿id].id == SKILL_MAGLEVEL: = %s", skills[item﻿id].id))
								required = voc:getRequiredManaSpent(player:getBaseMagicLevel() + 1)/skill_gain
								currently = player:getManaSpent()
								player:addManaSpent(required - currently)
							else
								print(string.format("ELSE skills[item﻿id].id == SKILL_MAGLEVEL: = %s", skills[item﻿id].id))
								required = voc:getRequiredSkillTries(skills[itemid].id, player:getSkillLevel(skills[itemid].id)+1)/skill_gain
								currently = player:getSkillTries(skills[itemid].id)

								player:addSkillTries(skills[itemid].id, (required - currently))
							end
							print(string.format("DEPOIS DO ELSE:"))

							fpos:sendMagicEffect(CONST_ME_HITAREA)
								print(string.format("fpos:sendMagicEffect:"))
							if skills[itemid].range then
								print(string.format("skills[itemid].range: %s", skills[itemid].range))
								pos_n:sendDistanceEffect(fpos, skills[itemid].range)
								print(string.format("DEPOIS skills[itemid].range: %s", skills[itemid].range))
							end
						    player:setStamina(player:getStamina() + 60)
						    print(string.format("DEPOIS player:getStamina(): %d", player:getStamina()))

						    if charges_n == 1 then 
								exercise:remove(1)
								return true
						    end
							local training = addEvent(start_train, voc:getAttackSpeed(), pid,start_pos,itemid,fpos)
						else
							exercise:remove(1)
							stopEvent(training)
						end
					end
				end
			end
		else
			stopEvent(training)
		end
	else
		stopEvent(training)
	end
	return true
end

function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	print(string.format("PET PLAYER: = %s", player))
	local start_pos = player:getPosition()
	print(string.format("PET PLAYER: = %d", target:getId()))
	local d﻿ummies2 = {32357,32358}

	if target:isItem() then
		--if isInArray(d﻿ummies2,target:getId()) then
		--if 32357 == target::getId() or 32358 == target::getId() then
			if not skills[item.itemid].range and (start_pos:getDistance(target:getPosition()) > 1) 
			then
				stopEvent(training)
				return false
			end
			if not player:getVocation():getId() == skills[item.itemid].voc or not player:getVocation():getId() == (skills[item.itemid].voc+4) then
				stopEvent(training)
				return false
			end
		    player:sendTextMessage(MESSAGE_STATUS_CONSOLE_BLUE, "You started training.")
			print(string.format("PET itemid: = %s", item))
			print(string.format("PET item.itemid: = %s", item.itemid))
			start_train(player:getId(),start_pos,item.itemid,target:getPosition())
		--end
	end

	return true
end