local config = {
	[60000] = {
		fromPosition = Position(32953, 32669, 7),
		toPosition = Position(32954, 32669, 7)
	},
	[60001] = {
		teleportPlayer = true,
		fromPosition = Position(32954, 32669, 7),
		toPosition = Position(32953, 32669, 7)
	}
}

function onUse(player, item, fromPosition, target, toPosition, isHotkey)
	local useItem = config[item.actionid]
	if not useItem then
		return true
	end

	if useItem.teleportPlayer then
		player:teleportTo(Position(32814, 32714, 11))
		player:getPosition():sendMagicEffect(CONST_ME_TELEPORT)
		player:say('This isnt a mirror but a portal. You can easily pass it.', TALKTYPE_MONSTER_SAY)
	end

	local tapestry = Tile(useItem.fromPosition):getItemById(9431)
	if tapestry then
		tapestry:moveTo(useItem.toPosition)
	end
	return true
end
